@extends('layouts.app')

@section('content')
    <p>You can now edit the data</p>
    {!! Form::open(['action' => ['blogController@update', $data-> id], 'method'=> 'POST']) !!}

    <!-- Title-->
    {{Form::label('title', 'Title')}}
    {{Form::text('title', $data->title) }}
    <!-- Body-->
    {{Form::label('body', 'Body')}}
    {{Form::label('body', $data->body, ['id'=>'article-ckeditor'])}}

    {{Form::hidden('_method','PUT')}}

    {!! Form::close() !!}

@endsection