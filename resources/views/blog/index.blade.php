@extends('layout.app')

@section('content')
    <p>Welcome & Have A Look!</p>
    @if(count($data)>0)
        @foreach($data as $blog)
        <div class="well">
            <h1><a href="/blog/{{$blog->id}}">{{$blog->title}}</a></h1>
            <small>Written on {{$blog->created_at}}</small>
        </div>
        @endfor
    @else
        <p>Oopss.. There's no blog post yet!</p>
    @endif

@endsection('content)
