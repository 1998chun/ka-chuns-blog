@extends('layout.app')

@section('content')
    <p>This is blog titled {{$data->title}}</p>
    <div>
        <h1>{{$data->title}}</a></h1>
        <small>Written on {{$data->created_at}}</small>
        <p>{{$data->body}}</p>
    </div>
    <button href="/blog">Go Back</button>

    <!--Edit function -->
    <button href="/blog/{{$data->id}}/edit" class="btn btn-dark">Edit</button>

    <!--destroy function -->
    {!!Form::open(['action' => ['blogController@destroy',$data->$id], 'method' => 'POST'])!!}}
    {{Form::hidden('_method',"DELETE")}}
    {{Form::submit('Delete',['class' => 'btn btn-danger'])}}
    {!!Form::close()!!}

@endsection('content)
