@extends('layout.app')

@section('content')
    <p>The Form should be viewing here</p>
    {!! Form::open(['action' => 'blogController@store', 'method'=>'POST'])!!}

    <!-- Title-->
    {{Form::label('title', 'Title')}}
    {{Form::text('title', '')}}
    <!-- Body-->
    {{Form::label('body', 'Body')}}
    {{Form::textarea('body', '',['id' => 'article-ckeditor'])}}

    {!! Form::close()}
@endsection('content)
