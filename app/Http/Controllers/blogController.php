<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\blog;

class blogController extends Controller
{

    public function index()
    {
        $data = blog::orderBy('created_at','desc')->get();
        return view('blog.index')->with('data',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this -> validate($request,[
            'title' => 'required',
            'body' => 'required',
        ]);

        $blog = new blog;
        $blog -> title = $request -> input('title');
        $blog -> body = $request -> input('body');
        $blog -> user_id = auth()->user()->id;
        $blog -> save();

        return redirect('/blog') ->with('success', 'blog Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = blog::find($id);
        return view('blog.show')->with('data',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = blog::find($id);
        return view('blog.edit') -> with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
           'title' => 'required',
            'body' => 'required',
        ]);

        $blog = blog::find($id);
        $blog -> title = $request -> input('title');
        $blog -> body = $request -> input('body');
        $blog -> save();

        return redirect('/blog') -> with('success', 'Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = blog::find($id);
        $blog -> delete();
        return redirect('/blog') -> with('success','Deleted Successfully!');
    }
}
